import Cookies from "js-cookie"
 
// state
export const state = () => ({
  user: null,
  token: null,
})
 
// getters
export const getters = {
  user: (state) => state.user,
  token: (state) => state.token,
  check: (state) => state.user !== null,
}
 
// mutations
export const mutations = {
  SET_TOKEN(state, token) {
    state.token = token
  },
 
  FETCH_USER_SUCCESS(state, user) {
    state.user = user
  },
 
  FETCH_USER_FAILURE(state) {
    state.token = null
  },
 
  LOGOUT(state) {
    state.user = null
    state.token = null
  },
 
  UPDATE_USER(state, { user }) {
    state.user = user
  },
}
 
// actions
export const actions = {
  saveToken({ commit }, { token, remember }) {
    if (token) {
      commit("SET_TOKEN", token)
      console.log("SET_TOKEN", token)
      Cookies.set("auth_token", token, { expires: remember ? 365 : null })
    } else {
      console.log("NOT SETTING EMPTY TOKEN")
    }
  },
 
  async fetchUser({ commit }) {
    try {
      const data = await this.$axios.$get("/auth/user")
 
      commit("FETCH_USER_SUCCESS", data)
    } catch (e) {
      Cookies.remove("auth_token")
 
      commit("FETCH_USER_FAILURE")
    }
  },
 
  updateUser({ commit }, payload) {
    commit("UPDATE_USER", payload)
  },
 
  async logout({ commit }) {
    try {
      await this.$axios.$post("/auth/logout")
    } catch (e) {}
 
    Cookies.remove("auth_token")
 
    commit("LOGOUT")
  },
}
