export default ({ store, redirect, route }) => {
  if (!store.getters['auth/check']) {
    window.previousPath = route.fullPath
    return redirect('/auth/login')
  }
}
