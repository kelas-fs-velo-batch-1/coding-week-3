<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Exam;
use App\Models\User;

class ExamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Exam::updateOrCreate([
            'exam_name' => 'UTS Programming',
        ], [
            'description' => 'Menguji keterampilan programming',
            'user_id' => User::where('username', 'test')->first()->id,
            'course_id' => '1',
            'available_from' => '2020-09-27 00:00:00',
            'available_to' => '2020-10-02 23:59:59',
        ]);
    }
}
